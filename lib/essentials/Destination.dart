class Destination {
  final String name;
  final String description;
  final List<String> resources;

  Destination({required this.name, required this.description, required this.resources});
}
