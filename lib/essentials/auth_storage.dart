import 'package:shared_preferences/shared_preferences.dart';

const String passwordKey = 'passwordKey';  // Define passwordKey as a constant

class AuthStorage {
  final SharedPreferences prefs;

  AuthStorage(this.prefs);

  String _getPasswordKey(String username) {
    return '$username$passwordKey';
  }

  String? getSavedUsername() {
    return prefs.getString('username');
  }

  String? getSavedPassword(String username) {
    return prefs.getString(_getPasswordKey(username));
  }

  Future<void> saveCredentials(String username, String password) async {
    prefs.setString('username', username);
    prefs.setString(_getPasswordKey(username), password);
  }

  Future<void> clearCredentials(String savedUsername) async {
    prefs.remove('username');
  }
}
