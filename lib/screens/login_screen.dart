import 'dart:math';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:trabuvist/screens/register_screen.dart';

import '../essentials/auth_storage.dart';
import 'home_screen.dart';

class LoginScreen extends StatefulWidget {
  final AuthStorage authStorage; // Pass the authStorage instance

  LoginScreen({required this.authStorage});

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  bool _isPasswordVisible = false;
  bool _rememberMe = false;

  late AuthStorage _authStorage;

  @override
  void initState() {
    super.initState();
    _initializeAuthStorage();
  }

  String generateRandomString(int length) {
    const String _chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    Random _rnd = Random();
    return String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length)),
    ));
  }

  void _initializeAuthStorage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _authStorage = AuthStorage(prefs);

    bool rememberMe = prefs.getBool('rememberMe') ?? false;
    setState(() {
      _rememberMe = rememberMe;
    });

    if (_rememberMe) {
      String? savedUsername = _authStorage.getSavedUsername();
      String? savedPassword = _authStorage.getSavedPassword(savedUsername!);
      if (savedPassword != null) {
        _usernameController.text = savedUsername;
        _passwordController.text = savedPassword;
        _handleLogin(savedUsername, savedPassword);
      }
    }
  }

  void _handleLogin(String username, String password) async {
    String? savedPassword = _authStorage.getSavedPassword(username);

    if (password == savedPassword) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => HomeScreen(onLogout: _handleLogout, authStorage: _authStorage)),
      );
    } else {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: const Text('Login Error'),
          content: const Text('Invalid credentials. Please try again.'),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text(
                'OK',
                style: TextStyle(color: Colors.purpleAccent),
              ),
            )
          ],
        ),
      );
    }
  }

  void _handleLogout() async {
    await widget.authStorage.clearCredentials(_usernameController.text);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('rememberMe', false);
    _rememberMe = false;
    _usernameController.clear();
    _passwordController.clear();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login'),
        backgroundColor: Colors.purple,
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextField(
                controller: _usernameController,
                decoration: const InputDecoration(
                  labelText: 'Username',
                  border: OutlineInputBorder(),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.purple),
                  ),
                  labelStyle: TextStyle(
                    fontSize: 18,
                    color: Colors.black,
                  ),
                ),
              ),
              const SizedBox(height: 16.0),
              TextField(
                controller: _passwordController,
                obscureText: !_isPasswordVisible,
                decoration: InputDecoration(
                  labelText: 'Password',
                  border: const OutlineInputBorder(),
                  focusedBorder: const OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.purple),
                  ),
                  labelStyle: const TextStyle(
                    fontSize: 18,
                    color: Colors.black,
                  ),
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        _isPasswordVisible = !_isPasswordVisible;
                      });
                    },
                    icon: Icon(_isPasswordVisible
                        ? Icons.visibility
                        : Icons.visibility_off),
                    color: Colors.black,
                  ),
                ),
              ),
              const SizedBox(height: 16.0),
              ElevatedButton(
                onPressed: () {
                  _handleLogin(_usernameController.text, _passwordController.text);
                },
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.purple,
                    textStyle: const TextStyle(
                      color: Colors.purpleAccent,
                    )
                ),
                child: const Text('Login'),
              ),
              const SizedBox(height: 16.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text('Don\'t have an account?'),
                  const SizedBox(width: 4.0),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => RegistrationScreen()),
                      );
                    },
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.purple,
                        textStyle: const TextStyle(
                          color: Colors.purpleAccent,
                        )
                    ),
                    child: const Text('Register'),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
