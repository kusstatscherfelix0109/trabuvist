import 'package:flutter/material.dart';

import '../essentials/Destination.dart';

class AddDestinationDialog extends StatefulWidget {
  @override
  _AddDestinationDialogState createState() => _AddDestinationDialogState();
}

class _AddDestinationDialogState extends State<AddDestinationDialog> {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();
  final TextEditingController resourcesController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Add Destination'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextField(
            controller: nameController,
            decoration: InputDecoration(labelText: 'Name'),
          ),
          TextField(
            controller: descriptionController,
            decoration: InputDecoration(labelText: 'Description'),
          ),
          TextField(
            controller: resourcesController,
            decoration: InputDecoration(labelText: 'Resources (comma separated)'),
          ),
        ],
      ),
      actions: [
        TextButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('Cancel'),
        ),
        ElevatedButton(
          onPressed: () {
            final name = nameController.text;
            final description = descriptionController.text;
            final resources = resourcesController.text.split(',');

            final newDestination = Destination(
              name: name,
              description: description,
              resources: resources,
            );

            Navigator.pop(context, newDestination);
          },
          child: Text('Add'),
        ),
      ],
    );
  }
}
