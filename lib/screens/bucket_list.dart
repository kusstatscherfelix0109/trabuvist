import 'package:flutter/material.dart';

import '../essentials/Destination.dart';
import '../essentials/auth_storage.dart';
import 'add_destination_dialog.dart';

class BucketListScreen extends StatefulWidget {
  final AuthStorage authStorage;
  final String username; // Add this line

  BucketListScreen({required this.authStorage, required this.username});


  @override
  _BucketListScreenState createState() => _BucketListScreenState();
}

class _BucketListScreenState extends State<BucketListScreen> {
  List<Destination> bucketList = [];

  @override
  void initState() {
    super.initState();
    loadBucketList();
  }

  void loadBucketList() async {
    final String? username = widget.authStorage.getSavedUsername();
    if (username != null) {
      // Load bucket list from storage
      // Example: bucketList = await loadBucketListFromStorage(username);
    }
  }

  void addToBucketList(Destination destination) async {
    // Add destination to bucket list
    bucketList.add(destination);

    final String? username = widget.authStorage.getSavedUsername();
    if (username != null) {
      // Save updated bucket list to storage
      // Example: await saveBucketListToStorage(username, bucketList);
    }

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Travel Bucket List')),
      body: ListView.builder(
        itemCount: bucketList.length,
        itemBuilder: (context, index) {
          final destination = bucketList[index];
          return ListTile(
            title: Text(destination.name),
            subtitle: Text(destination.description),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          // Show a dialog to add a destination
          final newDestination = await showDialog(
            context: context,
            builder: (context) => AddDestinationDialog(),
          );

          if (newDestination != null) {
            addToBucketList(newDestination);
          }
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
