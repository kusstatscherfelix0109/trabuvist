import 'package:flutter/material.dart';
import 'package:trabuvist/screens/login_screen.dart';
import '../essentials/auth_storage.dart';

class HomeScreen extends StatelessWidget {
  final VoidCallback onLogout;
  final AuthStorage authStorage;

  HomeScreen({required this.onLogout, required this.authStorage});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
        backgroundColor: Colors.purple,
        actions: [
          PopupMenuButton<String>(
            onSelected: (value) async {
              if (value == 'logout') {
                _logout(context);
              } else if (value == 'delete_account') {
                _deleteAccount(context);
              }
            },
            itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
              const PopupMenuItem<String>(
                value: 'logout',
                child: Text('Log Out'),
              ),
              const PopupMenuItem<String>(
                value: 'delete_account',
                child: Text('Delete Account'),
              )
            ],
          )
        ],
      ),
      body: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
        ),
        child: const Column(
          children: [
            Text(
              'Welcome to trabuvist',
              style: TextStyle(fontSize: 20, color: Colors.black),
            ),
          ],
        ),
      ),
    );
  }

  void _logout(BuildContext context) {
    onLogout();
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginScreen(authStorage: authStorage)),
    );
  }

  void _deleteAccount(BuildContext context) async {
    final bool confirmDelete = await showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: const Text('Delete Account'),
        content: const Text('Are you sure you want to delete your account?'),
        actions: [
          TextButton(
            onPressed: () async {
              final String? savedUsername = authStorage.getSavedUsername();
              if (savedUsername != null) {
                await authStorage.clearCredentials(savedUsername);
              }
              Navigator.pop(context, true);
            },
            child: const Text('Delete'),
          )
        ],
      ),
    );

    if (confirmDelete) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => LoginScreen(authStorage: authStorage)),
      );
    }
  }

}
