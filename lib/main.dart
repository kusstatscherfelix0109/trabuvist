import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'screens/login_screen.dart';
import 'essentials/auth_storage.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  AuthStorage authStorage = AuthStorage(prefs);

  runApp(MyApp(authStorage: authStorage));
}

class MyApp extends StatelessWidget {
  final AuthStorage authStorage;

  MyApp({required this.authStorage});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Trabuvist App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginScreen(authStorage: authStorage),
    );
  }
}
